package com.spartez.modules;

import com.spartez.modules.IMyModule;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IMyModuleModuleDescriptor extends AbstractModuleDescriptor<IMyModule>{
    private static final Logger log = LoggerFactory.getLogger(IMyModuleModuleDescriptor.class);

    public IMyModuleModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);
    }

    public IMyModule getModule()
    {
         return moduleFactory.createModule(moduleClassName, this);
    }

}