package ut.com.spartez;

public class Commit {
    final String tree;
    final String parent;
    final String id;


    public Commit(String tree, String parent, String id) {
        this.tree = tree;
        this.parent = parent;
        this.id = id;
    }


}
