package ut.com.spartez.modules;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.spartez.modules.IMyModuleModuleDescriptor;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class IMyModuleModuleDescriptorTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //IMyModuleModuleDescriptor testClass = new IMyModuleModuleDescriptor();

        throw new Exception("IMyModuleModuleDescriptor has no tests!");

    }

}
