package ut.com.spartez;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class GitAnalysis {

    public static Repository openJGitCookbookRepository(String path) throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        return builder
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir(new File(path)) // scan up the file system tree
                .build();
    }

    private static AbstractTreeIterator prepareTreeParser(Repository repository, String objectId) throws IOException {
        try (RevWalk walk = new RevWalk(repository)) {
            RevCommit commit = walk.parseCommit(repository.resolve(objectId));
            RevTree tree = walk.parseTree(commit.getTree().getId());

            CanonicalTreeParser treeParser = new CanonicalTreeParser();
            try (ObjectReader reader = repository.newObjectReader()) {
                treeParser.reset(reader, tree.getId());
            }

            walk.dispose();

            return treeParser;
        }
    }

    private String toString(ObjectId id) {
        String idString = id.toString();

        idString = idString.substring(12);
        idString = idString.substring(0, idString.length()-1);

        return idString;
    }

    private static void listDiff(Repository repository, Git git, String oldCommit, String newCommit) throws GitAPIException, IOException {
        final List<DiffEntry> diffs = git.diff()
                                         .setOldTree(prepareTreeParser(repository, oldCommit))
                                         .setNewTree(prepareTreeParser(repository, newCommit))
                                         .call();

        System.out.println("Found: " + diffs.size() + " differences");
        for (DiffEntry diff : diffs) {
            System.out.println("Diff: " + diff.getChangeType() + ": " +
                               (diff.getOldPath().equals(diff.getNewPath()) ? diff.getNewPath() : diff.getOldPath() + " -> " + diff.getNewPath()));
        }
    }
//
//    @Test
//    public void diff() throws Exception {
//        String repositoryId = "12";
//        String repoPath = String.format("./target/bitbucket/home/shared/data/repositories/%s/", repositoryId);
//        Repository repo = openJGitCookbookRepository(repoPath);
//        ObjectReader reader = repo.newObjectReader();
//        CanonicalTreeParser old = new CanonicalTreeParser();
//        old.reset(reader, ObjectId.fromString(treesIds.get(2)));
//        CanonicalTreeParser newTree = new CanonicalTreeParser();
//        newTree.reset(reader, ObjectId.fromString(treesIds.get(3)));
//        ByteArrayOutputStream os = new ByteArrayOutputStream();
//DiffFormatter df = new DiffFormatter( os );
//RawTextComparator cmp = RawTextComparator.DEFAULT;
//df.setRepository(repo);
//df.setDiffComparator(cmp);
//df.setDetectRenames(true);
//
//List<DiffEntry> diffEntries = df.scan("HEAD^{tree}", "HEAD^^{tree}");
//for (DiffEntry diffEntry : diffEntries) {
//   df.format(diffEntry);
//}
//System.out.println( df.toString() );
////52d9595e973214b93ffc57b46d493647d060253a
////b00e2ed7d9582863c42a7012c436eb1338d34fb4
//        System.out.println("END");
//    }

    int getIndexOfId(String commitId, List<String> ids) {
        for(int i = 0; i < ids.size(); i++) {
            if(ids.get(i).equals(commitId)) {
                return i;
            }
        }
        return -1;
    }

    String revToCommitId(String rev) {
        rev = rev.replace("commit ", "");
        rev = rev.substring(0, rev.indexOf(" "));
        return rev;
    }

    @Test
    public void trace() throws Exception {
        String initialLine = "Some dirty stuff happened here";
        String line = initialLine;
        String initialCommit = "";
        String repositoryId = "32";
        File f = new File(String.format("./target/bitbucket/home/shared/data/repositories/%s/", repositoryId));

//        Repository repo = new FileRepositoryBuilder().setGitDir(f).build();
        Git git = Git.open(f);
        Repository repo = git.getRepository();

        Ref master = repo.findRef("master");
        ObjectId masterTip = master.getObjectId();
        ObjectLoader loader = repo.open(masterTip);
        String obj = "";
        String parent = "";
        List<String> ids = new ArrayList<>();
        ids.add(toString(master.getObjectId()));
        RevWalk walk = new RevWalk(repo);
        do {
            obj = new String(loader.getBytes(), "UTF-8");
            parent = Arrays.stream(obj.split("\n")).filter(l -> l.startsWith("parent")).findFirst().map(p -> p.replace("parent", "").trim()).orElse(null);
            ids.add(parent);
            if (parent != null) {
                loader = repo.open(ObjectId.fromString(parent));
            }

        } while (parent != null);

        Map<String, List<String>> commitToChanges = new HashMap<>();
        List<String> lineHistory = new ArrayList<>();
        for(int i = 0; i < ids.size()-1; i++) {
            Process p = Runtime.getRuntime().exec(String.format("./diff.sh target/bitbucket/home/shared/data/repositories/%s %s %s", repositoryId, ids.get(i+1), ids.get(i)));
            List<String> out = new BufferedReader(new InputStreamReader(p.getInputStream())).lines().collect(Collectors.toList());
            commitToChanges.put(ids.get(i), out);
            for(int index = 0; index < out.size(); index++) {
                if(out.get(index).equals("+"+line)) {
                    if(out.get(index-1).startsWith("-")) {
                        line = out.get(index-1).substring(1);
                    }
                    lineHistory.add(line + "  =>  " + "COMMIT:"+ids.get(i));
//                    out.add(line);
//                    out.add("COMMIT:"+ids.get(i+1));
                }
            }
        }

//        List<String> history = commitToChanges.values().stream().map(list -> {
//                           if(list.size() > 1 && list.get(list.size()-1).contains("COMMIT")) {
//                               return list.get(list.size()-2)+"->"+list.get(list.size()-1);
//                           }
//                           return "";
//                       }).filter(l -> l.length() > 0)
//                       .collect(Collectors.toList());
        String firstOcurrence = lineHistory.get(lineHistory.size()-1);
        String firstCommit = firstOcurrence.substring(firstOcurrence.indexOf("COMMIT:")+"COMMIT:".length());
        RevCommit initial = walk.parseCommit(ObjectId.fromString(firstCommit)).getParents()[0];
        System.out.println("History of line: \"" + initialLine + "\" looks as follows:");
//        history.stream()
//               .map(l -> l.substring(0, l.indexOf("->")))
//               .forEach(System.out::println);
        lineHistory.forEach(System.out::println);
        RevCommit firstRev = walk.parseCommit(ObjectId.fromString(revToCommitId(initial.toString())));
        String initialMessage = firstRev.getFullMessage();
        String author = firstRev.getAuthorIdent().getName() + String.format("(%s)", firstRev.getAuthorIdent().getEmailAddress());
        System.out.println("First: " + initialMessage);
        System.out.println("Author: " + author);
//        System.out.println("Initial: " + initial);
//        String message = walk.parseCommit(ObjectId.fromString(firstCommitId)).getFullMessage();
//        String author = walk.parseCommit(ObjectId.fromString(firstCommitId)).getAuthorIdent().getName();
//        System.out.println("The reason why it's here is: " + message);
//        System.out.println("Author: " + author);
    }
}

/*
String parent = "";
do {
    parent = Arrays.stream(obj.split("\n")).filter(l -> l.startsWith("parent")).findFirst().orElse(null);
    if(parent != null) {
        parent = parent.replace("parent ", "");

    }

} while(parent != null);
 */


/*
Process p = Runtime.getRuntime().exec("./diff.sh target/bitbucket/home/shared/data/repositories/12 b00e2ed7d9582863c42a7012c436eb1338d34fb4 52d9595e973214b93ffc57b46d493647d060253a");
List<String> out = new BufferedReader(new InputStreamReader(p.getInputStream())).lines().collect(Collectors.toList());
List<String> changed = out.stream().filter(l -> l.startsWith("-") && !l.contains("---")).collect(Collectors.toList());
changed

 */

/*
for(int index = 0; index < ((ArrayList) out).size; index++) {
    if(out.get(index).equals("+"+line)) {
        line = out.get(index-1).substring(1);
        out.add(line);
        out.add("COMMIT:"+ids.get(i));
    }
}
 */

/*
List<String> history = commitToChanges.values().stream().map(list -> {
    if (list.size() > 1) {
        return list.get(list.size() - 2) + "->" + list.get(list.size() - 1);
    }
    return Collections.emptyList();
}).collect(Collectors.toList())

 */